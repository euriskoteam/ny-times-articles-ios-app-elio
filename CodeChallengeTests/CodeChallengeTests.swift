//
//  CodeChallengeTests.swift
//  CodeChallengeTests
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import XCTest
@testable import CodeChallenge

class CodeChallengeTests: XCTestCase {
    var apiService: APIService!
    var coreDataService: CoreDataService!

    override func setUp() {
        super.setUp()
        apiService = APIService.shared
        coreDataService = CoreDataService.shared
    }

    override func tearDown() {
        apiService = nil
        coreDataService = nil
        super.tearDown()
    }

    func testValidCallToNYAPI() {
        let promise = expectation(description: "API Returned Data")
        
        apiService.getMostViewedWeeklyArticlesForAllSections(success: { (statusCode, _) in
            XCTAssertEqual(statusCode, 200)
            promise.fulfill()
        }) { (error) in
            XCTFail()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testValidMappingFromNYAPI() {
        let promise = expectation(description: "Response Mapped Successfully")
        
        coreDataService.getMostViewedWeeklyArticlesForAllSections(success: { (result) in
            XCTAssertEqual(result, true)
            promise.fulfill()
        }) { (error) in
            XCTFail()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    /*
     Note that this call will fail because the NY API does not allow successive calls to its server, it will return a status code of 429: Too Many Requests
     */
    func testAPIPerformance() {
        self.measureMetrics([.wallClockTime], automaticallyStartMeasuring: true) {
            let promise = expectation(description: "API Returned Data")
            
            apiService.getMostViewedWeeklyArticlesForAllSections(success: { (statusCode, _) in
                XCTAssertEqual(statusCode, 200)
                promise.fulfill()
            }) { (error) in
                XCTFail()
            }
            
            waitForExpectations(timeout: 5, handler: { (_) in
                self.stopMeasuring()
            })
        }
    }
    
    func testCoreDataSavingPerformance() {
        let testBundle = Bundle(for: type(of: self))
        let path = testBundle.path(forResource: "Results", ofType: "json")
        let data = try? Data(contentsOf: URL(fileURLWithPath: path!), options: .alwaysMapped)
        XCTAssertNotNil(data)
        
        var wrapperModel: WrapperModel!
        do {
            wrapperModel = try JSONDecoder().decode(WrapperModel.self, from: data!)
        } catch {
            XCTFail()
        }
        
        self.measureMetrics([.wallClockTime], automaticallyStartMeasuring: true) {
            let promise = expectation(description: "Response Mapped Successfully")
            
            do {
                try coreDataService.saveModelToCoreData(wrapperModel)
                promise.fulfill()
            } catch {
                XCTFail()
            }
            
            waitForExpectations(timeout: 5, handler: { (_) in
                self.stopMeasuring()
            })
        }
    }

    /*
     Note that this call will fail because the NY API does not allow successive calls to its server, it will return a status code of 429: Too Many Requests
     */
    func testCoreDataAndAPIPerformance() {
        self.measureMetrics([.wallClockTime], automaticallyStartMeasuring: true) {
            let promise = expectation(description: "API Returned Data and Response Mapped Successfully")
            
            coreDataService.getMostViewedWeeklyArticlesForAllSections(success: { (result) in
                XCTAssertEqual(result, true)
                promise.fulfill()
            }) { (error) in
                XCTFail()
            }
            
            waitForExpectations(timeout: 5, handler: { (_) in
                self.stopMeasuring()
            })
        }
    }
}
