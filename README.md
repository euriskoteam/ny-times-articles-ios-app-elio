# README #

### What is this repository for? ###

To build a simple app that consumes the NY Times API and show a list of articles, that shows details when items on the list are tapped (a typical master/detail app) in Swift.

### How do I get set up? ###

* Summary of set up:
Just Launch the CodeChallenge.xcodeproj and run the app.
* Database configuration:
CoreData is implemented.
* How to run tests:
All XCTest and XCUITest are runnable by clicking on the diamond ring next to a given test function.
Note that two of the tests will likely fail because the NY API does not allow successive calls to its server, it will return a status code of 429: Too Many Requests.