//
//  CodeChallengeUITests.swift
//  CodeChallengeUITests
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import XCTest

class CodeChallengeUITests: XCTestCase {
    var app: XCUIApplication!

    override func setUp() {
        continueAfterFailure = false

        app = XCUIApplication()
        app.launch()
    }

    override func tearDown() {
        app = nil
        super.tearDown()
    }

    func testFunctionality() {
        let tableView = app.tables["MasterTableView"]
        XCTAssert(tableView.cells.count > 0)
        
        tableView.cells.firstMatch.tap()
        
        app.navigationBars["CodeChallenge.DetailView"].buttons["NY Times Most Popular"].tap()
        
        let randomPosition = Int.random(in: 0 ..< tableView.cells.count)
        let cell = tableView.cells["\(randomPosition)"]
        cell.firstMatch.tap()
    }
}
