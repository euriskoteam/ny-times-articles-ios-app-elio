//
//  DetailViewController.swift
//  CodeChallenge
//
//  Created by Eurisko on 2/12/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var imageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var detailTitleLabel: UILabel!
    @IBOutlet weak var detailByLabel: UILabel!
    @IBOutlet weak var detailDateLabel: UILabel!
    @IBOutlet weak var detailAbstractLabel: UILabel!
    
    var selectedArticle: Article?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let article = selectedArticle else { return }
        
        //1. Load Thumbnail Cached
        if let thumbnailMetaData = article.getThumbnail(), let urlString = thumbnailMetaData.url {
            detailImageView.cacheImage(urlString: urlString)
        }
        
        //2. Load Highest Quality Image and change ImageHeight 
        if let metaData = article.getHighestQualityImage(), let urlString = metaData.url {
            detailImageView.cacheImage(urlString: urlString, success: { [weak self] in
                self?.updateImageViewHeight(with: metaData)
            })
        }
        
        detailTitleLabel.text = article.title
        detailByLabel.text = article.byline
        detailDateLabel.text = article.publishedDate
        detailAbstractLabel.text = article.abstract
    }
    
    private func updateImageViewHeight(with imageMetaData: MetaData) {
        self.imageViewHeight.constant = (CGFloat(imageMetaData.height) * CGFloat(self.detailImageView.frame.width)) / CGFloat(imageMetaData.width)
        UIView.animate(withDuration: 0.4, delay: 0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}
