//
//  MasterTableViewCell.swift
//  CodeChallenge
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import UIKit

class MasterTableViewCell: UITableViewCell {
    @IBOutlet weak var articleImageView: UIImageView! {
        didSet {
            articleImageView.image = UIImage(named: "DefaultImage")
            articleImageView.layer.cornerRadius = 40
            articleImageView.clipsToBounds = true
        }
    }
    @IBOutlet weak var articleTitleLabel: UILabel!
    @IBOutlet weak var articleByLabel: UILabel!
    @IBOutlet weak var articleDateLabel: UILabel!
    
    // Prepare for reuse clears the old Article data before reusing the cell for a new Article
    override func prepareForReuse() {
        articleImageView.image = nil
        articleTitleLabel.text = nil
        articleByLabel.text = nil
        articleDateLabel.text = nil
    }
}
