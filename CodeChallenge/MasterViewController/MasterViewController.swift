//
//  MasterViewController.swift
//  CodeChallenge
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import UIKit
import CoreData

class MasterViewController: UIViewController {
    
    // Lazily Creating an NSFetchedResultsController instance to manage the CoreData Delegation
    lazy var fetchedResultsController: NSFetchedResultsController? = { () -> NSFetchedResultsController<Article>? in
        guard let moc = CoreDataStore.managedObjectContext else { return nil }
        let fetchRequest: NSFetchRequest<Article> = NSFetchRequest<Article>(entityName: "Article")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "publishedDate", ascending: true)]
        fetchRequest.fetchBatchSize = 10
        let _fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
        _fetchedResultsController.delegate = self
        return _fetchedResultsController
    }()
    
    lazy var refreshControl: UIRefreshControl = {
        let _refreshControl = UIRefreshControl()
        _refreshControl.addTarget(self, action: #selector(getData), for: UIControl.Event.valueChanged)
        _refreshControl.tintColor = UIColor.white
        return _refreshControl
    }()
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        try? fetchedResultsController?.performFetch()
        tableView.dataSource = self
        tableView.delegate = self
        getData()
        
        tableView.refreshControl = refreshControl
    }
    
    @objc func getData() {
        
        //Since CoreData Delegation manages the tableView Content, no need to do anything in the success block except ending the refresh
        CoreDataService.shared.getMostViewedWeeklyArticlesForAllSections(success: { [weak self] (_)  in
            self?.endRefreshing()
        }) { [weak self] (error) in
            self?.handleError(error)
        }
    }
    
    private func endRefreshing() {
        DispatchQueue.main.async {
            self.refreshControl.endRefreshing()
        }
    }
    
    // Present an AlertController to the user alerting him to the occurrence of an error, with the option to retry.
    private func handleError(_ error: Error?) {
        endRefreshing()
        
        let alertMessage = error?.localizedDescription ?? "Unknown Error Occurred."
        let alertController = UIAlertController(title: "Error", message: alertMessage, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(okAction)
        
        let retryAction = UIAlertAction(title: "Retry", style: .default) { [weak self] (_) in
            self?.getData()
        }
        alertController.addAction(retryAction)
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    // Initial configuration of the cell
    func configure(cell: UITableViewCell, at indexPath: IndexPath) {
        guard let cell = cell as? MasterTableViewCell, let currentArticle = fetchedResultsController?.object(at: indexPath) else { return }
        
        cell.articleTitleLabel.text = currentArticle.title
        cell.articleByLabel.text = currentArticle.byline
        cell.articleDateLabel.text = currentArticle.publishedDate
        if let thumbnailMetaData = currentArticle.getThumbnail(), let urlString = thumbnailMetaData.url {
            cell.articleImageView.cacheImage(urlString: urlString)
        }
    }
    
    // Reconfiguration of the cell whenever an update is made to CoreData
    func reconfigureCellAt(indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? MasterTableViewCell, let currentArticle = fetchedResultsController?.object(at: indexPath) else { return }
        
        cell.articleTitleLabel.text = currentArticle.title
        cell.articleByLabel.text = currentArticle.byline
        cell.articleDateLabel.text = currentArticle.publishedDate
        if let thumbnailMetaData = currentArticle.getThumbnail(), let urlString = thumbnailMetaData.url {
            cell.articleImageView.cacheImage(urlString: urlString)
        }
    }
    
    // Prepare for segue is called just before the segue is executed, we hand the selected article to the DetailViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destination = segue.destination as? DetailViewController, let selectedArticle = sender as? Article else { return }
        destination.selectedArticle = selectedArticle
    }
}

// UITableViewDataSource and UITableViewDelegate functions
extension MasterViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController?.sections?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = fetchedResultsController?.sections else { return 0 }
        let currentSection = sections[section]
        return currentSection.objects?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BodyCell") as? MasterTableViewCell ?? MasterTableViewCell()
        
        cell.accessibilityIdentifier = "\(indexPath.row)"
        configure(cell: cell, at: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let selectedArticle = fetchedResultsController?.object(at: indexPath) else { return }
        performSegue(withIdentifier: "DetailSegue", sender: selectedArticle)
    }
}

// NSFetchedResultsControllerDelegate functions
extension MasterViewController: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch(type) {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .left)
        case .update:
            guard let indexPath = indexPath else { return }
            reconfigureCellAt(indexPath: indexPath)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .left)
        case .move:
            break
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
}
