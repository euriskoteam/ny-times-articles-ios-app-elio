//
//  ArticleModel.swift
//  CodeChallenge
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import Foundation

struct ArticleModel: Decodable {
    var url: String?
    var adxKeywords: String?
    var column: String?
    var section: String?
    var byline: String?
    var type: String?
    var title: String?
    var abstract: String?
    var publishedDate: String?
    var source: String?
    var id: Int?
    var assetID: Int?
    var views: Int?
    var desFacet: [String]?
    var orgFacet: [String]?
    var perFacet: [String]?
    var geoFacet: [String]?
    var media: [MediaModel]?
    
    private enum CodingKeys: String, CodingKey {
        case url, column, section, byline, type, title, abstract, source, id, views, media
        case adxKeywords = "adx_keywords"
        case publishedDate = "published_date"
        case assetID = "asset_id"
        case desFacet = "des_facet"
        case orgFacet = "org_facet"
        case perFacet = "per_facet"
        case geoFacet = "geo_facet"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        url = try? values.decode(String.self, forKey: .url)
        adxKeywords = try? values.decode(String.self, forKey: .adxKeywords)
        column = try? values.decode(String.self, forKey: .column)
        section = try? values.decode(String.self, forKey: .section)
        byline = try? values.decode(String.self, forKey: .byline)
        type = try? values.decode(String.self, forKey: .type)
        title = try? values.decode(String.self, forKey: .title)
        abstract = try? values.decode(String.self, forKey: .abstract)
        publishedDate = try? values.decode(String.self, forKey: .publishedDate)
        source = try? values.decode(String.self, forKey: .source)
        id = try? values.decode(Int.self, forKey: .id)
        assetID = try? values.decode(Int.self, forKey: .assetID)
        views = try? values.decode(Int.self, forKey: .views)
        desFacet = try? values.decode([String]?.self, forKey: .desFacet) ?? []
        orgFacet = try? values.decode([String]?.self, forKey: .orgFacet) ?? []
        perFacet = try? values.decode([String]?.self, forKey: .perFacet) ?? []
        geoFacet = try? values.decode([String]?.self, forKey: .geoFacet) ?? []
        media = try? values.decode([MediaModel].self, forKey: .media)
    }
}
