//
//  WrapperModel.swift
//  CodeChallenge
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import Foundation

struct WrapperModel: Decodable {
    var status: String?
    var copyright: String?
    var numberOfResults: Int?
    var results: [ArticleModel]?
    
    private enum CodingKeys: String, CodingKey {
        case status, copyright, results
        case numberOfResults = "num_results"
    }
}
