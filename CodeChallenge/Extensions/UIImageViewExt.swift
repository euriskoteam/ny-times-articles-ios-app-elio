//
//  UIImageViewExt.swift
//  CodeChallenge
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import UIKit

extension UIImageView {
    func cacheImage(urlString: String, success: @escaping (() -> ()) = {  }) {
        let imageCache = APIService.shared.imageCache
        
        guard let url = URL(string: urlString) else { return }
        
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = imageFromCache
            success()
            return
        }
        
        URLSession.shared.dataTask(with: url) {
            data, response, error in
            if let data = data {
                DispatchQueue.main.async {
                    guard let imageToCache = UIImage(data: data) else { return }
                    imageCache.setObject(imageToCache, forKey: urlString as AnyObject)
                    self.image = imageToCache
                    success()
                }
            }
        }.resume()
    }
}
