//
//  MetaDataExtension.swift
//  CodeChallenge
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import Foundation

extension MetaData {
    
    // Only updating the model in case there is a change
    func updateFromModel(_ model: MetaDataModel) {
        if url != model.url { url = model.url }
        if format != model.format?.rawValue { format = model.format?.rawValue }
        if let modelHeight = model.height, height != Int32(modelHeight) { height = Int32(modelHeight) }
        if let modelWidth = model.width, width != Int32(modelWidth) { width = Int32(modelWidth) }
    }
}
