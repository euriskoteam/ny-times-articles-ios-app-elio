//
//  MediaExtension.swift
//  CodeChallenge
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import Foundation
import CoreData

extension Media {
    
    // Only updating the model in case there is a change
    func updateFromModel(_ model: MediaModel) {
        if type != model.type { type = model.type }
        if subtype != model.subtype { subtype = model.subtype }
        if caption != model.caption { caption = model.caption }
        if copyright != model.copyright { copyright = model.copyright }
        if let approvedForSyndication = model.approvedForSyndication, approvedForSyndication != approvedForSyndication { self.approvedForSyndication = Int32(approvedForSyndication) }
        
        // Since MetaData doesn't have a unique identifier, we can't push updates to it; Instead we have to delete all elements and reinsert them every time
        mediaMetaData = nil
        guard let moc = CoreDataStore.managedObjectContext else { return }
        model.mediaMetaData?.forEach({ (metaDataItem) in
            guard let entity = NSEntityDescription.entity(forEntityName: "MetaData", in: moc), let metaData = NSManagedObject(entity: entity, insertInto: moc) as? MetaData else { return }
            metaData.updateFromModel(metaDataItem)
            addToMediaMetaData(metaData)
        })
    }
}
