//
//  ArticleExtension.swift
//  CodeChallenge
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import Foundation
import CoreData

extension Article {
    
    // Only updating the model in case there is a change
    func updateFromModel(_ model: ArticleModel) {
        if url != model.url { url = model.url }
        if adxKeywords != model.adxKeywords { adxKeywords = model.adxKeywords }
        if column != model.column { column = model.column }
        if section != model.section { section = model.section }
        if byline != model.byline { byline = model.byline }
        if type != model.type { type = model.type }
        if title != model.title { title = model.title }
        if abstract != model.abstract { abstract = model.abstract }
        if publishedDate != model.publishedDate { publishedDate = model.publishedDate }
        if source != model.source { source = model.source }
        if let modelID = model.id, id != Int64(modelID) { id = Int64(modelID) }
        if let modelAsset = model.assetID, assetID != Int64(modelAsset) { assetID = Int64(modelAsset) }
        if let modelViews = model.views, views != Int32(modelViews) { views = Int32(modelViews) }
        if let desFacet = desFacet as? [String], desFacet != model.desFacet { self.desFacet = model.desFacet as NSObject? }
        if let orgFacet = orgFacet as? [String], orgFacet != model.orgFacet { self.orgFacet = model.orgFacet as NSObject? }
        if let perFacet = perFacet as? [String], perFacet != model.perFacet { self.perFacet = model.perFacet as NSObject? }
        if let geoFacet = geoFacet as? [String], geoFacet != model.geoFacet { self.geoFacet = model.geoFacet as NSObject? }
        
        // Since Media doesn't have a unique identifier, we can't push updates to it; Instead we have to delete all elements and reinsert them every time
        media = nil
        guard let moc = CoreDataStore.managedObjectContext else { return }
        model.media?.forEach({ (mediaItem) in
            guard let entity = NSEntityDescription.entity(forEntityName: "Media", in: moc), let media = NSManagedObject(entity: entity, insertInto: moc) as? Media else { return }
            media.updateFromModel(mediaItem)
            addToMedia(media)
        })
    }
    
    // Here we are looping through the MetaData array to get the Standard Thumbnail image
    func getThumbnail() -> MetaData? {
        guard let media = media else { return nil }
        for mediaItem in media {
            guard let mediaItem = mediaItem as? Media, mediaItem.type == "image" else { return nil }
            return mediaItem.mediaMetaData?.filter({ (metaItem) in
                guard let metaItem = metaItem as? MetaData else { return false }
                if metaItem.format == Format.standardThumbnail.rawValue {
                    return true
                }
                return false
            }).first as? MetaData
        }
        return nil
    }
    
    // Here we are looping through the MetaData array to get the Super Jumbo image
    func getHighestQualityImage() -> MetaData? {
        guard let media = media else { return nil }
        for mediaItem in media {
            guard let mediaItem = mediaItem as? Media, mediaItem.type == "image" else { return nil }
            return mediaItem.mediaMetaData?.filter({ (metaItem) in
                guard let metaItem = metaItem as? MetaData else { return false }
                if metaItem.format == Format.superJumbo.rawValue {
                    return true
                }
                return false
            }).first as? MetaData
        }
        return nil
    }
}
