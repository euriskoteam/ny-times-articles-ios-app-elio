//
//  CoreDataService.swift
//  CodeChallenge
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import Foundation
import CoreData

final class CoreDataService: NSObject {
    static let shared = CoreDataService()
    
    private override init() { }
    
    func getMostViewedWeeklyArticlesForAllSections(success: @escaping (Bool) -> (), failure: @escaping FailureBlock) {
        APIService.shared.getMostViewedWeeklyArticlesForAllSections(success: { (statusCode, wrapperModel) in
            if statusCode == 200, let wrapperModel = wrapperModel {
                DispatchQueue.main.async {
                    do {
                        try self.saveModelToCoreData(wrapperModel)
                        success(true)
                    } catch {
                        failure(error)
                    }
                }
            } else {
                success(false)
            }
        }, failure: failure)
    }
    
    // In this function, we search for an Article using its id; If we find it we call its update method, else we insert a new one
    func saveModelToCoreData(_ model: WrapperModel) throws {
        guard let moc = CoreDataStore.managedObjectContext else { return }
        try model.results?.forEach({ (articleModel) in
            guard let id = articleModel.id, let entity = NSEntityDescription.entity(forEntityName: "Article", in: moc) else { return }
            let fetchRequest: NSFetchRequest<Article> = Article.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "id = %li", id)
            fetchRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
            
            guard let article = try moc.fetch(fetchRequest).first ?? (NSManagedObject(entity: entity, insertInto: moc) as? Article) else { return }
            article.updateFromModel(articleModel)
        })
        try moc.save()
    }
}
