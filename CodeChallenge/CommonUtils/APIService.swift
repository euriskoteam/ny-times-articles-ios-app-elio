//
//  APIRequestor.swift
//  CodeChallenge
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko. All rights reserved.
//

import Foundation

enum APIMethod: String {
    case get = "GET"
    case post = "POST"
}

typealias SuccessBlock = (Int, WrapperModel?) -> Void
typealias FailureBlock = (Error?) -> Void

final class APIService: NSObject {
    static let shared = APIService()
    private var defaultSession = URLSession()
    private var dataTask = URLSessionDataTask()
    private let currentLink = "https://api.nytimes.com/svc/mostpopular/v2/mostviewed/all-sections/7.json?api-key=Rk6GfVjL9XA3A5ipo7bjr2fNh80CpeA5"
    
    let imageCache = NSCache<AnyObject, AnyObject>()
    
    private override init() {
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = 60
        config.timeoutIntervalForResource = 60
        defaultSession = URLSession(configuration: config)
    }
    
    //MARK: ~~~~~~~~ Make a Call to get the most viewed weekly articles for all sections ~~~~~~~~
    func getMostViewedWeeklyArticlesForAllSections(success: @escaping SuccessBlock = { _,_  in }, failure: @escaping FailureBlock) {
        
        guard let currentURL = URL(string: currentLink) else { return }
        let urlRequest = URLRequest(url: currentURL)
        dataTask = defaultSession.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
            if let error = error {
                failure(error)
                return
            }
            if let response = response as? HTTPURLResponse {
                if let data = data {
                    if response.statusCode == 200, let wrapperResponse = try? JSONDecoder().decode(WrapperModel.self, from: data) {
                        success(response.statusCode, wrapperResponse)
                        return
                    } else {
                        success(response.statusCode, nil)
                    }
                }
                failure(nil)
            }
        })
        
        dataTask.resume()
    }
}
